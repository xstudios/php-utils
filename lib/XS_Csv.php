<?php
/**
 * X Studios CSV class
 *
 * Allows easy output of CSV files that can be output as a
 * downloadable file or displayed in the browser.
 *
 * @author       Tim Santor <tsantor@xstudiosinc.com>
 * @version      1.0
 * @copyright    2013 X Studios
 * @link         http://www.xstudiosinc.com
 */
class XS_Csv {

    private $_fp;

    private $_column_headers = array();
    private $_column_rows    = array();

    // ------------------------------------------------------------------------
    // GETTERS / SETTERS
    // ------------------------------------------------------------------------

    /**
     * Sets the column headers
     *
     * @param     array  $headers  array of column headers
     * @return    void
     */
    public function setColumnHeaders($headers=array()) {
        if (empty($headers)) return;
        $this->_column_headers = $headers;
    }

    /**
     * Sets the column rows
     *
     * @param     array  $rows  array of all rows
     * @return    void
     */
    public function setColumnRows($rows=array()) {
        if (empty($rows)) return;
        $this->_column_rows = $rows;
    }

    // ------------------------------------------------------------------------

    /**
     * Constructor
     *
     * @param     array  $column_headers  array of column headers
     * @pram      array  $column_rows     array of all row data
     * @return    void
     */
    public function __construct($column_headers=array(), $column_rows=array()) {
        $this->_column_headers = $column_headers;
        $this->_column_rows    = $column_rows;
    }

    // ------------------------------------------------------------------------

    /**
     * Add a column row
     *
     * @param     array  $data  array of row data
     * @return    void
     */
    public function addRow($data=array()) {
        if (empty($data)) return;
        array_push($this->_column_rows, $data);
    }

    // ------------------------------------------------------------------------

    /**
     * Returns the CSV output
     *
     * @return    void
     */
    public function output() {
        // Create CSV file
        $contents = $this->_createFile();

        return $contents;
    }

    // ------------------------------------------------------------------------

    /**
     * Saves the CSV file as a file
     *
     * @param     string   $filepath
     * @return    boolean  true on success
     */
    public function save($filepath='/output') {
        // append file extension
        $filepath = $filepath.'.csv';

        // Create CSV file
        $contents = $this->_createFile($filepath);

        // write to the file
        if (fwrite($this->_fp, $contents)) {
            // Close open file pointer
            fclose($this->_fp);
            return true;
        }

        return false;
    }

    // ------------------------------------------------------------------------

    /**
     * Downloads the CSV file
     *
     * @param     string   $filename  the filename for the csv (no extension)
     * @return    void
     */
    public function download($filename='output') {
        // Output headers so that the file is downloaded rather than displayed
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename='.$filename.'.csv');

        // Create CSV file
        $this->_createFile();
    }

    // ------------------------------------------------------------------------

    /**
     * Outputs the CSV contents
     *
     * @param     string   $filename
     * @return    void
     */
    private function _createFile($filename='php://output') {
        // Create a file pointer
        $this->_fp = fopen($filename, 'w');

        // Output the column headers
        fputcsv($this->_fp, $this->_column_headers);

        // Output all the column rows
        foreach ($this->_column_rows as $_row) {
            fputcsv($this->_fp, $_row);
        }

        $contents = stream_get_contents($this->_fp);

        // Close open file pointer
        fclose($this->_fp);

        return $contents;
    }

    // ------------------------------------------------------------------------

}

/* End of file */
