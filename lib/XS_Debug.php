<?php
/**
 * X Studios Debug class
 *
 * Allows console debugging using javascript among
 * other helpful things like emailing logged errors, etc.
 *
 * @author       Tim Santor <tsantor@xstudiosinc.com>
 * @version      1.0
 * @copyright    2012 X Studios
 * @link         http://www.xstudiosinc.com
 */
class XS_Debug {

    const LOG   = 1;
    const INFO  = 2;
    const WARN  = 3;
    const ERROR = 4;

    private static $_debug_mode = true;
    private static $_debug_email;

    private static $_message_stack = array();

    // ------------------------------------------------------------------------
    // GETTERS / SETTERS
    // ------------------------------------------------------------------------
    public static function setDebugMode($value) { self::$_debug_mode = $value; }
    public static function setDebugEmail($value) { self::$_debug_email = $value; }

    // ------------------------------------------------------------------------

    /**
     * Trace to Console
     *
     * @param     string  $name  name, label, message
     * @param     var  $var  variable/array/object
     * @param     int  $type  (1)log, (2)info, (3)warn, (4)error
     * @return    void
     */
    public static function trace($name, $var=null, $type=1) {
        if ( self::$_debug_mode == false || self::_isUsingIE() == true ) return;
        echo '<script>'.PHP_EOL;
        switch($type) {
            case self::LOG:
                echo 'console.log('.json_encode($name).');'.PHP_EOL;
            break;
            case self::INFO:
                echo 'console.info('.json_encode($name).';'.PHP_EOL;
            break;
            case self::WARN:
                echo 'console.warn('.json_encode($name).');'.PHP_EOL;
            break;
            case self::ERROR:
                echo 'console.error('.json_encode($name).');'.PHP_EOL;
            break;
        }

        if (!empty($var)) {
            if (is_object($var) || is_array($var)) {
                $object = json_encode($var);
                echo 'var object'.preg_replace('~[^A-Z|0-9]~i',"_",$name).' = \''.str_replace("'","\'",$object).'\';'.PHP_EOL;
                echo 'var val'.preg_replace('~[^A-Z|0-9]~i',"_",$name).' = eval("(" + object'.preg_replace('~[^A-Z|0-9]~i',"_",$name).' + ")" );'.PHP_EOL;
                switch($type) {
                    case self::LOG:
                        echo 'console.debug(val'.preg_replace('~[^A-Z|0-9]~i',"_",$name).');'.PHP_EOL;
                    break;
                    case self::INFO:
                        echo 'console.info(val'.preg_replace('~[^A-Z|0-9]~i',"_",$name).');'.PHP_EOL;
                    break;
                    case self::WARN:
                        echo 'console.warn(val'.preg_replace('~[^A-Z|0-9]~i',"_",$name).');'.PHP_EOL;
                    break;
                    case self::ERROR:
                        echo 'console.error(val'.preg_replace('~[^A-Z|0-9]~i',"_",$name).');'.PHP_EOL;
                    break;
                }
            } else {
                switch($type) {
                    case self::LOG:
                        echo 'console.debug("'.str_replace('"','\\"',$var).'");'.PHP_EOL;
                    break;
                    case self::INFO:
                        echo 'console.info("'.str_replace('"','\\"',$var).'");'.PHP_EOL;
                    break;
                    case self::WARN:
                        echo 'console.warn("'.str_replace('"','\\"',$var).'");'.PHP_EOL;
                    break;
                    case self::ERROR:
                        echo 'console.error("'.str_replace('"','\\"',$var).'");'.PHP_EOL;
                    break;
                }
            }
        }
        echo '</script>'.PHP_EOL;
    }

    // ------------------------------------------------------------------------

    /**
     * Add Message
     *
     * @param     string  $name  name, label, message
     * @param     var  $var  variable/array/object
     * @param     int  $type  (1)log, (2)info, (3)warn, (4)error
     * @param     bool  $log  log to text file
     * @return    void
     */
    public static function addMessage($name, $var=null, $type=1, $log=false) {
        if ($log == true) self::logError($name);

        if ( self::$_debug_mode == false) return;

        array_push(self::$_message_stack, array('name'=>$name, 'var'=>$var, 'type'=>$type));
    }

    // ------------------------------------------------------------------------

    /**
     * Debug Dump
     *
     * Outputs all debug messages to the console
     *
     * @return    void
     */
    public static function debugDump() {
        if ( self::$_debug_mode == false) return;

        foreach(self::$_message_stack as $message) {
            self::trace($message['name'], $message['var'], $message['type']);
        }

        // clear the message stack
        self::$_message_stack = array();
    }

    // ------------------------------------------------------------------------

    /**
     * Write Error to Text File
     *
     * @param     string   $error       error message to log
     * @param     boolean  $send_email  send email
     * @return    boolean
     */
    public static function logError($error, $send_email=false) {
        $filename = 'xs_error_log.txt';

        // ip address and user agent
        $ip_address = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
        $user_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';

        // content to insert into file
        $content  = '['.date('Y-m-d h:i:s').'] '.$error.PHP_EOL;
        $content .= '['.$ip_address.'] '.$user_agent.PHP_EOL;
        $content .= '// ------------------------------------------------------------------------'.PHP_EOL;

        // create the log file if one doesn't exist
        if (!file_exists($filename)) {
            $handle = fopen($filename, 'w');
            fclose($handle);
            chmod($filename, 0755);
        }

        // let's make sure the file exists and is writable first
        if (is_writable($filename)) {

            // get the file size
            $bytes = filesize($filename);

            // determine write mode by seeing if it's bigger than 10MB
            $write_mode = $bytes >= 10485760 ? 'w' : 'a'; // write or append

            // try to open our file in correct write mode
            if (!$handle = fopen($filename, $write_mode)) {
                 self::addMessage(__METHOD__.':: Cannot open file ('.$filename.')');
                 return;
            }

            // write error to our opened file
            if (fwrite($handle, $content) === FALSE) {
                self::addMessage(__METHOD__.':: Cannot write to file ('.$filename.')');
                return;
            }

            // email the error to developer if debug mode is false or we force send email
            //if ( self::$_debug_mode == false || $send_email == true ) {
            if ( $send_email == true ) {
                self::emailDebugInfo($error);
            }

            // close our file
            fclose($handle);

        } else {
            self::addMessage(__METHOD__.':: The file ('.$filename.') is not writable.');
            // fallback to default PHP error logging method
            error_log($error);
        }
    }

    // ------------------------------------------------------------------------

    /**
     * Email Debug Info
     *
     * @param     string  $error  error message
     * @return    void
     */
    public static function emailDebugInfo($error='') {
        // if no email, bail...
        if ( empty(self::$_debug_email) ) return;

        // domain, domain ip, timestamp, ip address, user agent
        $domain_name = $_SERVER['SERVER_NAME'];
        $domain_ip   = isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : '?';
        $timestamp   = date('Y-m-d h:i:s');
        $ip_address  = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '?';
        $user_agent  = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '?';

        // start backtrace with some info
        $backtrace  = '<strong>DOMAIN:</strong> %s &mdash; %s<br/>';
        $backtrace .= '<strong>TIMESTAMP:</strong> %s<br/>';
        $backtrace .= '<strong>USER IP:</strong> %s<br/>';
        $backtrace .= '<strong>USER AGENT:</strong> %s<br/><br/>';
        $backtrace  = sprintf($backtrace, $domain_name, $domain_ip, $timestamp, $ip_address, $user_agent);

        if ( !empty($error) ) $backtrace .= '<strong>MESSAGE:</strong> '.$error.'<br/><br/>';

        // add GET/POST/SESSION data for help with debugging
        if ( !empty($_GET) )     $backtrace .= '<strong>GET:</strong><pre>'.print_r($_GET, 1).'</pre>';
        if ( !empty($_POST) )    $backtrace .= '<strong>POST:</strong><pre>'.print_r($_POST, 1).'</pre>';
        if ( !empty($_SESSION) ) $backtrace .= '<strong>SESSION:</strong><pre>'.print_r($_SESSION, 1).'</pre>';

        // create email message
        $message = $backtrace;

        // send email
        $headers = 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        error_log($message, 1, self::$_debug_email, $headers);
    }

    // ------------------------------------------------------------------------

    /**
     * Peak Memory Usage
     *
     * Won't attempt to execute if debug mode is false
     *
     * @param     string $method method called from
     * @return    boolean
     */
    public static function peakMemoryUsage($method='') {
        if (self::$_debug_mode == true) {
            $mb = round(memory_get_peak_usage()/1048576, 1);
            self::addMessage($method.':: Peak memory usage ('.$mb.'MB)');
        }
    }

    // ------------------------------------------------------------------------

    /**
     * Pretty Print
     *
     * Echos a print_r() wrapped in <pre> tags for a variable's value or if
     * many arguments are passed, it will echo each value.
     *
     * @param     mixed $var,...  variable name
     * @return    void
     */
    public static function prettyPrint($var) {
        $args = func_get_args();
        $html = '';
        foreach ($args as $value) {
            if (is_array($value) || is_object($value)) {
                $html .= '<pre>'.print_r($value, 1).'</pre>'.PHP_EOL;
            } else {
                $html .= $value.'<br/>';
            }
        }
        echo $html;
    }

    // ------------------------------------------------------------------------

    /**
     * Return an exception string with simply the stuff we need. No traceback.
     *
     * @param     Exception  $e
     * @return    string
     */
    public static function simpleException($e) {
        return $e->getMessage() . ' on ' . $e->getFile() . ':' . $e->getLine();
    }

    // ------------------------------------------------------------------------

    /**
     * URL Information
     *
     * Echos a print_r() wrapped in <pre> tags with all information on
     * the URL (scheme, host, path, query).
     *
     * @param     string  $url
     * @return    void
     */
    public static function urlInfo($url) {
        $url = parse_url($url);
        parse_str($url['query'], $query_array);
        $url['query_params'] = $query_array;
        self::prettyPrint($url);
    }

    // ------------------------------------------------------------------------
    // PRIVATE METHODS
    // ------------------------------------------------------------------------

    /**
     * Return if the Browser is IE
     *
     * So we can not try to output traces to console
     * Not fullproof by any means, but ok for now...
     *
     * @return boolean
     */
    private static function _isUsingIE() {
        if ( isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false) ) {
            return true;
        }else{
            return false;
        }
    }

    // ------------------------------------------------------------------------

}

/* End of file */
