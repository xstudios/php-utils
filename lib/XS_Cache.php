<?php
/**
 * X Studios Cache Helper
 *
 * Helpers are wrapped in a class to avoid name collisions with other
 * 3rd party helper methods as well as native PHP methods
 *
 * @author      Tim Santor <tsantor@xstudios.agency>
 * @version     1.0
 * @copyright   2014 X Studios
 * @link        http://xstudios.agency
 */

class XS_Cache {


    // ------------------------------------------------------------------------

    /**
     * File
     *
     * Get a new filename to cache based on it's last modified time stamp.
     *
     * @access    public
     * @param     string $fileName
     * @return    string
     */
    public static function file($fileName) {
        if (file_exists($fileName))
        {
            $lastModified = filemtime($fileName);
            $pathParts = pathinfo($fileName);
            $fileName = $pathParts['dirname'].'/'.$pathParts['filename'].'.'.$lastModified.'.'.$pathParts['extension'];
        }
        return $fileName;
    }

    // ------------------------------------------------------------------------

}

/* End of file */
