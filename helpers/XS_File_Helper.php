<?php
/**
 * X Studios File Helper
 *
 * Helps with  file operations
 *
 * Helpers are wrapped in a class to avoid name collisions with other
 * 3rd party helper methods as well as native PHP methods
 *
 * @author       Tim Santor <tsantor@xstudiosinc.com>
 * @version      1.0
 * @copyright    2012 X Studios
 * @link         http://www.xstudiosinc.com
 */
class XS_File_Helper {

    // ------------------------------------------------------------------------

    /**
     * Read File
     *
     * Opens the file specfied in the path and returns it as a string.
     *
     * @access    public
     * @param     string  $file  path to file
     * @return    string
     */
    public static function read($file) {
        if ( ! file_exists($file)) {
            return false;
        }

        if (function_exists('file_get_contents')) {
            return file_get_contents($file);
        }

        if ( ! $fp = @fopen($file, FOPEN_READ)) {
            return false;
        }

        flock($fp, LOCK_SH);

        $data = '';
        if (filesize($file) > 0) {
            $data =& fread($fp, filesize($file));
        }

        flock($fp, LOCK_UN);
        fclose($fp);

        return $data;
    }

    // ------------------------------------------------------------------------

    /**
     * Write File
     *
     * Writes data to the file specified in the path.
     * Creates a new file if non-existent.
     *
     * @access    public
     * @param     string  $path  path to file
     * @param     string  $data  file data
     * @param     string  $mode  r, r+, w, w+, a, a+
     * @return    bool
     */
    public static function write($path, $data, $mode='a') {
        if ( ! $fp = @fopen($path, $mode)) {
            return false;
        }

        flock($fp, LOCK_EX);
        fwrite($fp, $data.PHP_EOL);
        flock($fp, LOCK_UN);
        fclose($fp);

        return true;
    }

    // ------------------------------------------------------------------------

    /**
     * Delete Files and/or Folders
     *
     * Deletes all files contained in the supplied directory path.
     * With great power comes great responsibility.
     *
     * @access    public
     * @param     string  $path  path to file
     * @param     bool  $del_dir  delete any directories found in the path
     * @param     int  $level  set to 1 in order to delete target dir
     * @return    bool
     */
    public static function delete_files($path, $del_dir=false, $level=0) {
        $path = rtrim($path, DIRECTORY_SEPARATOR);

        if ( ! $current_dir = @opendir($path)) {
            return false;
        }

        while (false !== ($filename = @readdir($current_dir))) {
            if ($filename != "." and $filename != "..") {
                if (is_dir($path.DIRECTORY_SEPARATOR.$filename)) {
                    // ignore empty folders
                    if (substr($filename, 0, 1) != '.') {
                        self::delete_files($path.DIRECTORY_SEPARATOR.$filename, $del_dir, $level + 1);
                    }
                } else {
                    unlink($path.DIRECTORY_SEPARATOR.$filename);
                }
            }
        }
        @closedir($current_dir);

        if ($del_dir == true && $level > 0) {
            return @rmdir($path);
        }

        return true;
    }

    // ------------------------------------------------------------------------

    /**
     * Delete File
     *
     * With great power comes great responsibility.
     *
     * @access    public
     * @param     string  $file  file
     * @return    bool
     */
    public static function delete($file) {
        @unlink($file);
    }

    // ------------------------------------------------------------------------

    /**
     * Get File Info
     *
     * Given a file and path, returns the name, path, size, date modified
     * You can specifiy what information you want returned. Options are:
     * name, server_path, size, date, readable, writable, executable, fileperms
     * Returns false if the file cannot be found.
     *
     * @access    public
     * @param     string  $file  path to file
     * @param     array  $returned_values  information returned
     * @return    array
     */
    public static function get_file_info($file, $returned_values=array('name', 'server_path', 'size', 'date')) {
        if ( ! file_exists($file)) {
            return false;
        }

        foreach ($returned_values as $key) {
            switch ($key) {
                case 'name':
                    $name = substr(strrchr($file, DIRECTORY_SEPARATOR), 1);
                    $fileinfo['name'] = $name ? $name : $file;
                    break;
                case 'server_path':
                    $fileinfo['server_path'] = $file;
                    break;
                case 'size':
                    $fileinfo['size'] = filesize($file);
                    break;
                case 'date':
                    $fileinfo['date'] = filemtime($file);
                    break;
                case 'readable':
                    $fileinfo['readable'] = is_readable($file);
                    break;
                case 'writable':
                    // There are known problems using is_writable on IIS.
                    // It may not be reliable - consider fileperms()
                    $fileinfo['writable'] = is_writable($file);
                    break;
                case 'executable':
                    $fileinfo['executable'] = is_executable($file);
                    break;
                case 'fileperms':
                    $fileinfo['fileperms'] = fileperms($file);
                    break;
            }
        }

        return $fileinfo;
    }

    // ------------------------------------------------------------------------

    /**
     * Get Path Info
     *
     * @access    public
     * @param     string  $file  path to file
     * @return    array
     */
    public static function get_path_info($file) {
        $info = pathinfo($file);
        // add base filename for anything less than PHP 5.2.0
        if (!isset($info['filename'])) {
            $info['filename'] = basename($file,'.'.$info['extension']);
        }

        return $info;
    }

    // ------------------------------------------------------------------------

    /**
     * Get Realpath
     *
     * @access    public
     * @param     string  $path
     * @param     bool  $check_existance  checks to see if the path exists
     * @return    string
     */
    public static function get_realpath($path, $check_existance=false) {
        // security check to make sure the path is NOT a URL.  No remote file inclusion!
        $pattern = "#^(http:\/\/|https:\/\/|www\.|ftp|[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})#i";
        if (preg_match($pattern, $path)) {
            trigger_error('The path you submitted must be a local server path, not a URL');
        }

        // resolve the path
        if (function_exists('realpath') && @realpath($path) !== false) {
            $path = realpath($path).'/';
        }

        // add a trailing slash
        $path = preg_replace("#([^/])/*$#", "\\1/", $path);

        // make sure the path exists
        if ($check_existance == true) {
            if ( ! is_dir($path)) {
                trigger_error('Not a valid path: '.$path);
            }
        }

        return $path;
    }

    // ------------------------------------------------------------------------

    /**
     * Recursive delete directory
     *
     * @access    public
     * @param     string  $directory
     * @param     bool  $empty leave the top level dir, but empty its contents
     * @return    boolean
     */
    public static function rmdir($directory, $empty=false)
    {
        // if the path has a slash at the end we remove it here
        if(substr($directory,-1) == '/')
        {
            $directory = substr($directory,0,-1);
        }

        // if the path is not valid or is not a directory ...
        if(!file_exists($directory) || !is_dir($directory))
        {
            // ... we return false and exit the function
            return false;

        // ... if the path is not readable
        }elseif(!is_readable($directory))
        {
            // ... we return false and exit the function
            return false;

        // ... else if the path is readable
        }else{

            // we open the directory
            $handle = opendir($directory);

            // and scan through the items inside
            while (false !== ($item = readdir($handle)))
            {
                // if the filepointer is not the current directory
                // or the parent directory
                if($item != '.' && $item != '..')
                {
                    // we build the new path to delete
                    $path = $directory.'/'.$item;

                    // if the new path is a directory
                    if(is_dir($path))
                    {
                        // we call this function with the new path
                        self::rmdir($path);

                    // if the new path is a file
                    }else{
                        // we remove the file
                        unlink($path);
                    }
                }
            }
            // close the directory
            closedir($handle);

            // if the option to empty is not set to true
            if($empty == false)
            {
                // try to delete the now empty directory
                if(!rmdir($directory))
                {
                    // return false if not possible
                    return false;
                }
            }
            // return success
            return true;
        }
    }

    // ------------------------------------------------------------------------

}

/* End of file */
