<?php
/**
 * X Studios String Helper
 *
 * Helps manipulate strings
 *
 * Helpers are wrapped in a class to avoid name collisions with other
 * 3rd party helper methods as well as native PHP methods
 *
 * @author       Tim Santor <tsantor@xstudios.agency>
 * @version      1.0
 * @copyright    2011 X Studios
 * @link         http://www.xstudios.agency
 */
class XS_String_Helper {

    // ------------------------------------------------------------------------

    /**
     * Word Limiter
     *
     * Limits a string to X number of words.
     *
     * @access    public
     * @param     string
     * @param     integer
     * @param     string the end character. Usually an ellipsis
     * @return    string
     */
    public static function limit_words($str, $limit, $end_char='...;') {
        if (trim($str) == '') return $str;

        preg_match('/^\s*+(?:\S++\s*+){1,'.(int) $limit.'}/', $str, $matches);

        if (strlen($str) == strlen($matches[0])) $end_char = '';

        return rtrim($matches[0]).$end_char;
    }

    // ------------------------------------------------------------------------

    /**
     * Character Limiter
     *
     * Limits the string based on the character count.  Preserves complete words
     * so the character count may not be exactly as specified.
     *
     * @access    public
     * @param     string
     * @param     integer
     * @param     string the end character. Usually an ellipsis
     * @return    string
     */

    public static function limit_chars($str, $limit, $end_char='...') {
        if (strlen($str) < $limit) return $str;

        $str = preg_replace("/\s+/", ' ', str_replace(array("\r\n", "\r", "\n"), ' ', $str));

        if (strlen($str) <= $limit) return $str;

        $out = "";
        foreach (explode(' ', trim($str)) as $val) {
            $out .= $val.' ';

            if (strlen($out) >= $limit) {
                $out = trim($out);
                return (strlen($out) == strlen($str)) ? $out : $out.$end_char;
            }
        }
    }

    // ------------------------------------------------------------------------

    /**
     * Random String
     *
     * Create a random string
     *
     * @access    public
     * @param     int $minlength a number
     * @param     int $maxlength a number
     * @param     boolean $useupper a boolean
     * @param     boolean $usespecial a boolean
     * @param     boolean $usenumbers a boolean
     * @return    string
     */
    public static function random_string($minlength, $maxlength, $useupper, $usespecial, $usenumbers) {
        $charset = "abcdefghijklmnopqrstuvwxyz";
        if ($useupper) $charset .= "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        if ($usenumbers) $charset .= "0123456789";
        if ($usespecial) $charset .= "~@#$%^*()_+-={}|][";
        // Note: using all special characters this reads: "~!@#$%^&*()_+`-={}|\\]?[\":;'><,./";
        if ($minlength > $maxlength) {
            $length = mt_rand ($maxlength, $minlength);
        } else {
            $length = mt_rand ($minlength, $maxlength);
        }
        $key = '';
        for ($i=0; $i<$length; $i++) {
            $key .= $charset[(mt_rand(0,(strlen($charset)-1)))];
        }
        return $key;
    }

    // ------------------------------------------------------------------------

    /**
     * Get String Between
     *
     * Get string between 2 strings
     *
     * @access    public
     * @param     string $str a string
     * @param     string $start a string
     * @param     string $end a string
     * @return    string
     */
    public static function get_string_between($str, $start, $end){
        $str = " ".$str;
        $ini = strpos($str,$start);
        if ($ini == 0) return "";
        $ini += strlen($start);
        $len = strpos($str,$end,$ini) - $ini;
        return substr($str,$ini,$len);
    }

    // ------------------------------------------------------------------------

    /**
     * String Match with Wildcard
     *
     * Determine if a string matches the wildcard search
     *
     * @access    public
     * @param     string $source a string
     * @param     string $pattern a string
     * @return    bool
     */
    public static function string_match_with_wildcard($source, $pattern) {
        $pattern = preg_quote($pattern,'/');
        $pattern = str_replace( '\*' , '.*', $pattern);
        return (bool)preg_match( '/^' . $pattern . '$/i' , $source );
    }

    // ------------------------------------------------------------------------

    /**
     * Strip Line Breaks
     *
     * Strips line breaks such as \n and \r
     *
     * @access    public
     * @param     string $str a string
     * @return    string
     */
    public static function strip_line_breaks($str) {
        $str = str_replace(array('\n', '\r'),' ', $str);
        return $str;
    }

    // ------------------------------------------------------------------------

    /**
     * Trim Slashes
     *
     * Removes any leading/trailing slashes from a string
     *
     * @access    public
     * @param     string
     * @return    string
     */
    public static function trim_slashes($str) {
        return trim($str, '/');
    }

    // ------------------------------------------------------------------------

    /**
     * Strip Slashes
     *
     * Removes slashes contained in a string or in an array
     *
     * @access    public
     * @param     mixed string or array
     * @return    mixed string or array
     */
    public static function strip_slashes($str) {
        if (is_array($str)) {
            foreach ($str as $key => $val) {
                $str[$key] = strip_slashes($val);
            }
        } else {
            $str = stripslashes($str);
        }

        return $str;
    }

    // ------------------------------------------------------------------------

    /**
     * Strip Quotes
     *
     * Removes single and double quotes from a string
     *
     * @access    public
     * @param     string
     * @return    string
     */
    public static function strip_quotes($str) {
        $str = self::clean_word_string($str);
        return str_replace(array('"', "'"), '', $str);
    }

    // ------------------------------------------------------------------------

    /**
     * Quotes to Entities
     *
     * Converts single and double quotes to entities
     *
     * @access    public
     * @param     string
     * @return    string
     */
    public static function quotes_to_entities($str) {
        $str = self::clean_word_string($str);
        return str_replace(array("\'","\"","'",'"'), array("&#39;","&quot;","&#39;","&quot;"), $str);
    }

    // ------------------------------------------------------------------------

    /**
     * Clean MS Word string
     *
     * Replace Microsoft Word version of single and double quotations marks
     * (“ ” ‘ ’) with  regular quotes (' and ")
     *
     * @access    public
     * @param     string
     * @return    string
     */
    public static function clean_word_string($str) {
        return iconv(mb_detect_encoding($str), 'ASCII//TRANSLIT', $str);
    }

    // ------------------------------------------------------------------------

    /**
     * Reduce Double Slashes
     *
     * Converts double slashes in a string to a single slash,
     * except those found in http://
     *
     * @access    public
     * @param     string
     * @return    string
     */
    public static function reduce_double_slashes($str) {
        return preg_replace("#(^|[^:])//+#", "\\1/", $str);
    }

    // ------------------------------------------------------------------------

    /**
     * Returns a boolean for typical boolean strings
     *
     * "1", "true",  "on"  and "yes" will return true
     * "0", "false", "off" and "no"  will return false
     *
     * @access    public
     * @param     string
     * @return    boolean
     */
    public static function to_boolean($str) {
        switch (strtolower($str)) {
            case '1':
            case 'true':
            case 'on':
            case 'yes':
            case 'active':
            case 'enabled':
                return true;
                break;
            case '0':
            case 'false':
            case 'off':
            case 'no':
            case 'inactive':
            case 'disabled':
                return false;
                break;
            default:
                return false;
        }
    }

    // ------------------------------------------------------------------------

    /**
     * Remove all special characters but a hyphen unless specified.
     *
     * @access    public
     * @param     string
     * @return    string
     */
    public static function remove_special_chars($str, $include_hyphen=false) {
        $str = preg_replace('/[^A-Za-z0-9-\s]/', '', $str);
        if ($include_hyphen) {
            $str = str_replace('-', '', $str);
        }
        return $str;
    }

    // ------------------------------------------------------------------------

    /**
     * Return only numbers.
     *
     * Money, social security numbers , phone numbers, etc.
     *
     * @access    public
     * @param     string
     * @return    string
     */
    public static function clean_number($str, $allow_decimal=true) {
        if ($allow_decimal) {
            $str = preg_replace('/[^0-9\.]/', '', $str);
        } else {
            $str = preg_replace('/[^0-9]/', '', $str);
        }
        return $str;
    }

    // ------------------------------------------------------------------------

    /**
     * Returns true if a string contains another string
     *
     * @access    public
     * @param     string
     * @param     string
     * @return    boolean
     */
    public static function contains($needle, $haystack) {
        if (strpos($haystack, $needle) !== false) {
            return true;
        }

        return false;
    }

    // ------------------------------------------------------------------------

    /**
     * Camelize
     *
     * Takes multiple words separated by spaces or underscores
     * and camelizes them
     *
     * @access    public
     * @param     string
     * @return    string
     */
    public static function camelize($str) {
        $str = preg_replace("/[^a-zA-Z0-9_-\s]/i", '', $str);
        $str = 'x'.strtolower(trim($str));
        $str = ucwords(preg_replace('/[\s_-]+/', ' ', $str));
        return substr(str_replace(' ', '', $str), 1);
    }

    // ------------------------------------------------------------------------

    /**
     * Underscorize
     *
     * Takes multiple words separated by spaces and '_' separates them
     *
     * @access    public
     * @param     string
     * @return    string
     */
    public static function underscorize($str) {
        return preg_replace('/[\s]+/', '_', strtolower(trim($str)));
    }

    // ------------------------------------------------------------------------

    /**
     * Humanize
     *
     * Takes multiple words separated by underscores and changes
     * them to spaces
     *
     * @access    public
     * @param     string
     * @return    string
     */
    public static function humanize($str) {
        return ucwords(preg_replace('/[\\._-]+/', ' ', strtolower(trim($str))));
    }

    // ------------------------------------------------------------------------

    /**
     * Dasherize
     *
     * Takes multiple words separated by spaces and '-' separates them
     *
     * @access    public
     * @param     string
     * @return    string
     */
    public static function dasherize($str) {
        $str = preg_replace("/[^a-zA-Z0-9_-\s]/i", '', $str);
        $str = str_replace(' ', '-', trim($str));
        $str = preg_replace("/-+/i", '-', $str);
        $str = strtolower($str);

        return $str;
    }

    // ------------------------------------------------------------------------

    /**
     * Dotterize
     *
     * Takes multiple words separated by spaces and '.' separates them
     *
     * @access    public
     * @param     string
     * @return    string
     */
    public static function dotterize($str) {
        $str = preg_replace("/[^a-zA-Z0-9_-\s]/i", '', $str);
        $str = str_replace(' ', '.', trim($str));
        $str = preg_replace("/\.+/i", '.', $str);
        $str = strtolower($str);

        return $str;
    }

    // ------------------------------------------------------------------------

}

/* End of file */
