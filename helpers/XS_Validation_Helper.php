<?php
/**
 * X Studios Validation Helper
 *
 * Helps with server side validations
 *
 * Helpers are wrapped in a class to avoid name collisions with other
 * 3rd party helper methods as well as native PHP methods
 *
 * @author       Tim Santor <tsantor@xstudiosinc.com>
 * @version      1.0
 * @copyright    2012 X Studios
 * @link         http://www.xstudiosinc.com
 */
class XS_Validation_Helper {

    // ------------------------------------------------------------------------
    // PERSONAL INFO
    // ------------------------------------------------------------------------

    /**
     * Validate a Date of Birth
     *
     * Ensure user is the required age or older
     *
     * @access    public
     * @param     string  $date
     * @param     int  $min_age  minumum age allowed
     * @return    bool
     */
    public static function is_age($date='now', $min_age=18) {
        $age = intval(substr(date('Ymd') - date('Ymd', strtotime($date)), 0, -4));
        if ($age >= $min_age) {
            return true;
        }
        return false;
    }

    // ------------------------------------------------------------------------

    /**
     * Validate a date
     *
     * Matches dates in the format of:
     * 01/01/2012, 01-01-2012, 01.01.2012 or 01 01 2012
     * -- or --
     * 2012/01/01, 2012-01-01, 2012.01.01 or 2012 01 01
     *
     * @access    public
     * @param     string  $date
     * @return    bool
     */
    public static function is_date($date) {
        // mm-dd-yyyy
        $pattern1 = "/^(0[1-9]|1[012])[ -.\\/](0[1-9]|[12][0-9]|3[01])[ -.\\/](19|20)[0-9]{2}$/";
        // yyyy-mm-dd
        $pattern2 = "/^(19|20)[0-9]{2}[ -.\\/](0[1-9]|1[012])[ -.\\/](0[1-9]|[12][0-9]|3[01])$/";
        if ( self::_is_match($pattern1, $date) || self::_is_match($pattern2, $date) ) {
            return true;
        }
        return false;
    }

    // ------------------------------------------------------------------------

    /**
     * Validate email address
     *
     * @access    public
     * @param     string  $email
     * @return    bool
     */
    public static function is_email($email) {
        $pattern = "/^([a-z0-9\+_\-\.]{3,64})@([a-z0-9\-]{3,63}\.)+[a-z]{2,3}$/i";
        return self::_is_match($pattern, $email);
    }

    // ------------------------------------------------------------------------

    /**
     * Validate a US based phone number
     *
     * Matches a 10 digit number in the format of:
     * 3334445555, 333.444.5555, 333-444-5555, 333 444 5555 or (333) 444 5555
     * and all combinations thereof
     *
     * @access    public
     * @param     string  $phone
     * @return    bool
     */
    public static function is_phone($phone) {
        $pattern = "/^\(?[0-9]{3}\)?[-. ]?[0-9]{3}[-. ]?[0-9]{4}$/";
        return self::_is_match($pattern, $phone);
    }

    // ------------------------------------------------------------------------

    /**
     * Validate a US zip code
     *
     * Matches zip codes in the format of:
     * 12345, 12345-6789 or 12345 6789
     *
     * @access    public
     * @param     string  $zip_code
     * @return    bool
     */
    public static function is_zip($zip_code) {
        $pattern = "/^([0-9]{5})([- ][0-9]{4})?$/i";
        return self::_is_match($pattern, $zip_code);
    }

    // ------------------------------------------------------------------------

    /**
     * Validate an address
     *
     * @access    public
     * @param     string  $address
     * @return    bool
     */
    public static function is_address($address) {
        $pattern = "/^\d+\w+\s\w+\s\w+[.]?$/i";
        return self::_is_match($pattern, $address);
    }

    // ------------------------------------------------------------------------

    /**
     * Validate a Social Security Number
     *
     * Matches 9 digit numbers in the format of:
     * 123456789, 123.45.6789, 123-45-6789 or 123 45 6789
     *
     * @access    public
     * @param     string  $ssn
     * @return    bool
     */
    public static function is_ssn($ssn) {
        $pattern = "/^[0-9]{3}[-. ]?[0-9]{2}[-. ]?[0-9]{4}$/";
        return self::_is_match($pattern, $ssn);
    }

    // ------------------------------------------------------------------------

    /**
     * Validate if the number is USD
     *
     * Matches USD in the format of:
     * 1500, 1500.00, 1,1500 or 1,1500.00
     *
     * @access    public
     * @param     int  $card_num
     * @return    bool
     */
    public static function is_usd($amount) {
        $pattern = "/^[\$]?[0-9]{1,3}(?:,?[0-9]{3})*(?:\.[0-9]{2})?$/";
        return self::_is_match($pattern, $amount);
    }

    // ------------------------------------------------------------------------
    // MISC
    // ------------------------------------------------------------------------

    /**
     * Validate an IP address
     *
     * Matches 0.0.0.0 through 255.255.255.255
     *
     * @access    public
     * @param     string  $ip
     * @return    bool
     */
    public static function is_ip_address($ip) {
        $pattern = "/^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/";
        return self::_is_match($pattern, $ip);
    }

    // ------------------------------------------------------------------------

    /**
     * Validate a URL
     *
     * Matches http or https in the format of:
     * http://www.xstudiosinc.com or http://xstudiosinc.com
     *
     * @access    public
     * @param     string  $url
     * @return    bool
     */
    public static function is_url($url) {
        $pattern = "/^(http|https|ftp):\/\/(([A-Z0-9][A-Z0-9_-]*)(\.[A-Z0-9][A-Z0-9_-]*)+)(:(\d+))?$/i";
        return self::_is_match($pattern, $url);
    }

    // ------------------------------------------------------------------------

    /**
     * Validate a Hex color
     *
     * Matches a 24 bit hex color in the format of:
     * #fff or #ffffff
     *
     * @access    public
     * @param     string  $url
     * @return    bool
     */
    public static function is_hex($hex) {
        $pattern = "/^#[0-9A-F]{3,6}$/i";
        return self::_is_match($pattern, $hex);
    }

    // ------------------------------------------------------------------------
    // PRIVATE METHODS
    // ------------------------------------------------------------------------

    /**
     * Test a pattern against the given text
     *
     * @access    public
     * @param     string  $pattern
     * @param     string  $text
     * @return    bool
     */
    private static function _is_match($pattern, $text) {
        if ( preg_match($pattern, $text) ) {
            return true;
        }
        return false;
    }

    // ------------------------------------------------------------------------

}

/* End of file */
