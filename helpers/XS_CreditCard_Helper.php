<?php
/**
 * X Studios Validation Helper
 *
 * Helps with server side validations
 *
 * Helpers are wrapped in a class to avoid name collisions with other
 * 3rd party helper methods as well as native PHP methods
 *
 * @author       Tim Santor <tsantor@xstudiosinc.com>
 * @version      1.0
 * @copyright    2012 X Studios
 * @link         http://www.xstudiosinc.com
 */
class XS_CreditCard_Helper {

    // ------------------------------------------------------------------------

    /**
     * Determines if a credit card number passes the Luhn mod-10 checksum.
     *
     * @access    public
     * @param     int  $card_num
     * @return    bool
     */
    public static function is_valid($card_num) {
        $card_number_checksum = '';

        foreach (str_split(strrev((string) $card_num)) as $i => $d) {
            $card_number_checksum .= $i %2 !== 0 ? $d * 2 : $d;
        }

        return array_sum(str_split($card_number_checksum)) % 10 === 0;
    }

    // ------------------------------------------------------------------------

    /**
     * Validate a American Express card
     *
     * Determines if a number is a Amex card. Numbers start with `34` or `37`.
     *
     * Matches 15 digit numbers in the format of:
     * 37xxxxxxxxxxxxx
     * 37xx xxxx xxxx xxx
     * 37xx-xxxx-xxxx-xxx
     * 37xx.xxxx.xxxx.xxx
     *
     * @access    public
     * @param     int  $card_num
     * @return    bool
     */
    public static function is_amex($card_num) {
        if (self::is_valid($card_num) == false) {
            return false;
        }

        $pattern = "/^3[47][0-9]{2}[ -]?[0-9]{4}[ -]?[0-9]{4}[ -]?[0-9]{3}$/";
        return self::_is_match($pattern, $card_num);
    }

    // ------------------------------------------------------------------------

    /**
     * Validate a Discover card
     *
     * Determines if a number is a Discover card. Numbers start with `6011`.
     *
     * Matches 16 digit numbers in the format of:
     * 6011xxxxxxxxxxxx
     * 6011 xxxx xxxx xxxx
     * 6011-xxxx-xxxx-xxxx
     * 6011.xxxx.xxxx.xxxx
     *
     * @access    public
     * @param     string  $card_num
     * @return    bool
     */
    public static function is_discover($card_num) {
        if (self::is_valid($card_num) == false) {
            return false;
        }

        $pattern = "/^6011[ -]?[0-9]{4}[ -]?[0-9]{4}[ -]?[0-9]{4}$/";
        return self::_is_match($pattern, $card_num);
    }

    // ------------------------------------------------------------------------

    /**
     * Validate a MasterCard
     *
     * Determines if a number is a Master card. Numbers start with `51`-`55`.
     *
     * Matches 16 digit numbers in the format of:
     * 54xxxxxxxxxxxxxx
     * 54xx xxxx xxxx xxxx
     * 54xx-xxxx-xxxx-xxxx
     * 54xx.xxxx.xxxx.xxxx
     *
     * @access    public
     * @param     int  $card_num
     * @return    bool
     */
    public static function is_master($card_num) {
        if (self::is_valid($card_num) == false) {
            return false;
        }

        $pattern = "/^5[1-5][0-9]{2}[ -]?[0-9]{4}[ -]?[0-9]{4}[ -]?[0-9]{4}$/";
        return self::_is_match($pattern, $card_num);
    }

    // ------------------------------------------------------------------------

    /**
     * Validate a Visa card
     *
     * Determines if a number is a Visa card. Numbers start with a `4`.
     *
     * Matches 13 or 16 digit numbers in the format of:
     * 4xxxxxxxxxxxxxxx
     * 4xxx xxxx xxxx xxxx
     * 4xxx-xxxx-xxxx-xxxx
     * 4xxx.xxxx.xxxx.xxxx
     *
     * @access    public
     * @param     int  $card_num
     * @return    bool
     */
    public static function is_visa($card_num) {
        if (self::is_valid($card_num) == false) {
            return false;
        }

        $pattern = "/^4[0-9]{3}[ -]?[0-9]{4}[ -]?[0-9]{4}[ -]?[0-9]{4}|4[0-9]{3}[ -]?[0-9]{4}[ -]?[0-9]{4}[ -]?[0-9]{1}$/";
        return self::_is_match($pattern, $card_num);
    }

    // ------------------------------------------------------------------------

    /**
     * Return credit card type
     *
     * Matches all major cards
     *
     * @access    public
     * @param     int  $card_num
     * @return    bool
     */
    public static function card_type($card_num) {
        if (self::is_amex($card_num)) {
            return 'AX';
        }
        if (self::is_discover($card_num)) {
            return 'DS';
        }
        if (self::is_master($card_num)) {
            return 'MC';
        }
        if (self::is_visa($card_num)) {
            return 'VI';
        }

        return 'N/A';
    }

    // ------------------------------------------------------------------------
    // PRIVATE METHODS
    // ------------------------------------------------------------------------

    /**
     * Test a pattern against the given text
     *
     * @access    public
     * @param     string  $pattern
     * @param     string  $text
     * @return    bool
     */
    private static function _is_match($pattern, $text) {
        if ( preg_match($pattern, $text) ) {
            return true;
        }
        return false;
    }

    // ------------------------------------------------------------------------

}

/* End of file */
