<?php
/**
 * X Studios Array Helper
 *
 * Helps manipulate arrays
 *
 * Helpers are wrapped in a class to avoid name collisions with other
 * 3rd party helper methods as well as native PHP methods
 *
 * @author      Tim Santor <tsantor@xstudiosinc.com>
 * @version     1.0
 * @copyright   2012 X Studios
 * @link        http://www.xstudiosinc.com
 */

class XS_Array_Helper {

    /**
     * Required for sorting methods
     *
     * @var  string
     */
    private static $_key;

    // ------------------------------------------------------------------------

    /**
     * Element
     *
     * Lets you determine whether an array index is set and whether it has a value.
     * If the element is empty it returns FALSE (or whatever you specify as the default value.)
     *
     * @access    public
     * @param     string $item array key
     * @param     array $array array
     * @param     mixed $default
     * @return    mixed
     */
    public static function element($item, $array, $default=false) {
        if ( isset($array[$item]) == false || empty($array[$item]) == true) {
            return $default;
        }

        return $array[$item];
    }

    // ------------------------------------------------------------------------

    /**
     * Elements
     *
     * Returns a new array of only the array items specified.
     *
     * @access    public
     * @param     array $items
     * @param     array $array
     * @param     mixed $default
     * @return    mixed
     */
    public static function elements($items, $array, $default=false) {
        $return = array();

        if ( !is_array($items)) {
            $items = array($items);
        }

        foreach ($items as $item) {
            if (isset($array[$item])) {
                $return[$item] = $array[$item];
            } else {
                $return[$item] = $default;
            }
        }

        return $return;
    }

    // ------------------------------------------------------------------------

    /**
     * Random Element
     *
     * Returns a random element from an array
     *
     * @access    public
     * @param     array $array
     * @return    mixed
     */
    public static function random_element($array) {
        if ( !is_array($array)) {
            return $array;
        }

        return $array[array_rand($array)];
    }

    // ------------------------------------------------------------------------

    /**
     * Recursively converts object to an array
     *
     * Returns an array
     *
     * @access    public
     * @param     object $obj
     * @return    array
     */
    public static function object_to_array($obj) {

        if (is_object($obj)) {
            $obj = (array) $obj;
        }

        if (is_array($obj)) {
            $new = array();
            foreach($obj as $key => $val) {
                $new[$key] = self::object_to_array($val);
            }
        }
        else {
            $new = $obj;
        }

        return $new;
    }

    // ------------------------------------------------------------------------

    /**
     * Recursively converts array to an object
     *
     * Returns an object
     *
     * @access    public
     * @param     array $arr
     * @return    array
     */
    public static function array_to_object($arr) {

        $new = new stdClass();
        foreach ($arr as $k => $v) {
            if (is_array($v)) {
                $new->$k = self::array_to_object($v);
            }
            else {
                $new->$k = $v;
            }
        }

        return $new;
    }

    // ------------------------------------------------------------------------

    /**
     * Sort multidimensional array by key ascending
     *
     * @access    public
     * @param     array   $array
     * @param     string  $key
     * @return    array
     */
    public static function sort_by_key_asc($array, $key) {
        self::$_key = $key;
        usort($array, self::compareAB);
        return $array;
    }

    private static function compareAB($a, $b) {
        return strcmp($a[self::$_key], $b[self::$_key]);
    }

    // ------------------------------------------------------------------------

    /**
     * Sort multidimensional array by key desc
     *
     * @access    public
     * @param     array   $array
     * @param     string  $key
     * @return    array
     */
    public static function sort_by_key_desc($array, $key) {
        self::$_key = $key;
        usort($array, self::compareBA);
        return $array;
    }

    private static function compareBA($a, $b) {
        return strcmp($b[self::$_key], $a[self::$_key]);
    }

    // ------------------------------------------------------------------------

}

/* End of file */
